#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import socket

count = 0

n = {"name":"test2", "data":[45, 12312, 23, 123]}

sock = socket.socket()
data = json.dumps(n)
sock.connect(('localhost', 9090))

while count < 5:
    # Connect to server and send data
    sock.sendall(bytes(data, encoding="utf-8"))

    # Receive data from the server and shut down
    received = sock.recv(1024)
    received = received.decode("utf-8")
    print(received)
    count += 1

sock.close()

