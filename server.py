#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import socket

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(1)
conn, addr = sock.accept()

print('connected:', addr)

while True:
    raw_data = conn.recv(2048).decode("utf-8")
    if not raw_data:
        break
    print(raw_data)
    data = json.loads(raw_data)
    data["processed"] = True
    data = json.dumps(data)
    conn.sendall(bytes(data, encoding="utf-8"))

conn.close()